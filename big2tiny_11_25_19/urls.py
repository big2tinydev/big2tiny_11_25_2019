from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from big2tiny_11_25_19 import settings, views

urlpatterns = [
    path('', views.index, name='home'),
    path('admin/', admin.site.urls),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
